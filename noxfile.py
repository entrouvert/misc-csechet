"""Nox configuration file"""
from os import fsdecode
from pathlib import Path

import nox
from nox import Session

nox.options.sessions = ["checks"]

LINT_PATHS = ["frontools", "tests"]
PYTEST_PACKAGES = ["pytest", "pytest-cov", "pytest-datadir"]
POETRY_PACKAGES = ["poetry", "poetry-dynamic-versioning"]
VENV_DIR = Path(".venv")


@nox.session(python=["3.9", "3.10"], reuse_venv=True)
def tests(session: Session) -> None:
    """Run unit tests."""
    session.install("-e", ".", *PYTEST_PACKAGES)
    session.run("pytest")


@nox.session(reuse_venv=True)
def black(session: Session) -> None:
    """Check black formatting."""
    session.install("black")
    session.run("black", "--check", *LINT_PATHS)


@nox.session(reuse_venv=True)
def flake8(session: Session) -> None:
    """Run flake8"""
    session.install("flake8")
    session.run("flake8", "frontools", "tests")


@nox.session(reuse_venv=True)
def isort(session: Session) -> None:
    """Check imports sorting"""
    session.install("isort")
    session.run("isort", "--check", *LINT_PATHS)


@nox.session(reuse_venv=True)
def mypy(session: Session) -> None:
    """Run Mypy"""
    session.install("mypy", "types-pyyaml", "types-click", "-e", ".")
    session.run("mypy", "--install-types", *LINT_PATHS)


@nox.session(reuse_venv=True)
def pylint(session: Session) -> None:
    """Run pylint"""
    session.install("pylint", "-e", ".", *PYTEST_PACKAGES)
    session.run("pylint", "--rcfile=pyproject.toml", *LINT_PATHS)


@nox.session(python=False)
def lint(session: Session) -> None:
    """Run all lint tasks"""
    session.notify("black")
    session.notify("flake8")
    session.notify("isort")
    session.notify("mypy")
    session.notify("pylint")


@nox.session(python=False)
def checks(session: Session) -> None:
    """Run all checks"""
    session.notify("lint")
    session.notify("tests")


@nox.session(python=False)
def dev(session: Session) -> None:
    """
    Sets up a python development environment for the project.
    """

    session.run(
        "python3",
        "-m",
        "virtualenv",
        "-p",
        "python3.10",
        fsdecode(VENV_DIR),
        silent=True,
    )
    python_path = Path.cwd() / VENV_DIR / "bin/python"
    python = fsdecode(python_path)
    session.run(
        python, "-m", "pip", "install", *POETRY_PACKAGES, external=True, silent=True
    )
    session.run(python, "-m", "poetry", "install", external=True, silent=True)


@nox.session(python="3.10")
def publish(session: Session) -> None:
    """
    Publish a package to pypi
    """

    session.install(*POETRY_PACKAGES)
    session.run("poetry", "publish", "--build")
