"""frontools main module"""
from asyncio import run
from functools import update_wrapper
from logging import INFO, basicConfig, getLogger
from pathlib import Path
from typing import Any, Optional

from click import Context as ClickContext
from click import Path as PathArgument
from click import argument, group, option, pass_context, pass_obj

from frontools.config import Config
from frontools.misc import open_urls
from frontools.screenshot import screenshot
from frontools.sources import CachedSource


def _async_command(function: Any) -> Any:
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        return run(function(*args, **kwargs))

    return update_wrapper(wrapper, function)


_LOGGER = getLogger(__file__)


@group()
@pass_context
@option(
    "-c",
    "--config-file",
    type=PathArgument(exists=True, file_okay=True, dir_okay=False),
    default=None,
    metavar="FILE",
    help="Configuration file to use",
)
@option(
    "-s",
    "source",
    type=str,
    required=True,
    default="remote",
    help="Source to use (configured in config file)",
)
@option("--no-cache", type=bool, help="Disable caching", count=True)
@option("-u", "--update-index", type=bool, help="Reload cached theme index", count=True)
@option(
    "-i",
    "--include-tags",
    type=str,
    multiple=True,
    help="Take into account only urls with those tags",
)
@option(
    "-e",
    "--exclude-tags",
    type=str,
    multiple=True,
    help="Ignore urls matching with those tags",
)
@_async_command
async def main(
    ctx: ClickContext,
    config_file: Optional[Path],
    source: str,
    no_cache: bool,
    update_index: bool,
    include_tags: list[str],
    exclude_tags: list[str],
) -> None:
    """Utilities for EO frontend development."""
    basicConfig(format="%(message)s", level=INFO)
    config = await Config.load(
        config_file,
        update_index,
        source,
        not no_cache,
        include_tags,
        exclude_tags,
    )

    def _on_close() -> None:
        processed_urls: list[tuple[str, str]] = list(config.urls)
        all_themes: set[str] = set(theme for theme, _ in ctx.obj.all_urls)
        processed_themes: set[str] = set(theme for theme, _ in processed_urls)
        ignored_themes: set[str] = all_themes - processed_themes
        _LOGGER.info(
            f"Processed {len(processed_urls)} urls belonging to {len(processed_themes)} out of {len(all_themes)} themes"
        )
        if len(ignored_themes):
            _LOGGER.info("Following themes were skipped :")
            for theme in ignored_themes:
                _LOGGER.info(theme)

    ctx.obj = config
    ctx.call_on_close(_on_close)


@main.command(name="prune-caches")
@argument("cache_names", nargs=-1)
def prune_caches(cache_names: list[str]) -> None:
    """Prune frontools caches"""
    CachedSource.prune(cache_names)


@main.command(name="screenshot")
@option("-o", "--output-directory", type=PathArgument(), default=None)
@option("-w", "--screen-width", type=int, default=None)
@option(
    "-c",
    "--continue",
    "continue_",
    type=bool,
    help="Don't reshot already existing screenshots",
    count=True,
)
@_async_command
@pass_obj
async def screenshot_cli(
    config: Config,
    output_directory: Optional[str],
    screen_width: Optional[int],
    continue_: bool,
) -> None:
    """Generate screenshot diffs"""
    await screenshot(
        config, output_directory, screen_width=screen_width, continue_=continue_
    )


@main.command(name="open")
@argument("url_pattern", type=str, required=True)
@_async_command
@pass_obj
async def open_cli(
    config: Config,
    url_pattern: str,
) -> None:
    """Generate screenshot diffs"""
    await open_urls(config, url_pattern)


if __name__ == "__main__":
    main()  # pylint: disable=no-value-for-parameter
