"""Commands to open pages"""
from re import compile as re_compile

from frontools.config import Config


async def open_urls(config: Config, pattern: str) -> None:
    re_pattern = re_compile(pattern)

    async with config.source.get_browser(headless=False) as browser:
        urls = [url for _, url in config.urls if re_pattern.match(url)]

        for url in urls:
            async with browser.load_page(url) as page:
                await page.pause()
